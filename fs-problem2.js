const fs = require("fs"); //importing fs module
const fileNamePath = "../fileNames.txt"; //setting file path
function readFile(path, callBack) {
  //creating function
  fs.readFile(path, "utf8", (error, data) => {
    //reading data inside txt file
    if (error) {
      callBack(error); //if error occurs it will call callBack function
    } else {
      let convertedData1 = data.toUpperCase(); //converting all characters to upperCase

      fs.writeFile("../upperCaseData.txt", convertedData1, "utf8", (error) => {
        //adding data inside new file using writeFile method
        if (error) {
          callBack(error);
        }
        fs.appendFile(fileNamePath, "upperCaseData.txt\n", "utf8", (error) => {
          //appending file name inside my fileNames.txt file
          if (error) {
            callBack(error);
          }
          fs.readFile("../upperCaseData.txt", "utf8", (error, data) => {
            //reading data inside txt file
            if (error) {
              callBack(error);
            }
            let convertedData2 = data.toLowerCase(); //converting back to lowerCase.
            let convertedResult = convertedData2.split("\n").join(" "); //splitting string and joining array
            fs.writeFile(
              //adding data inside new file using writeFile method
              "../seperatedByLine.txt",
              convertedResult,
              "utf8",
              (error) => {
                if (error) {
                  callBack(error);
                }
                fs.appendFile(
                  //appending file name inside my fileNames.txt file
                  fileNamePath,
                  "seperatedByLine.txt\n",
                  "utf8",
                  (error) => {
                    if (error) {
                      callBack(error);
                    }
                    fs.readFile(
                      //reading data inside txt file
                      "../seperatedByLine.txt",
                      "utf8",
                      (error, data) => {
                        if (error) {
                          callBack(error);
                        }
                        let words = data.split(" "); //splitting string with respect to space
                        let sortedWords = words.sort(); //sorting array
                        let sortedSentence = sortedWords.join(" "); //joining back
                        fs.writeFile(
                          //adding data inside new file using writeFile method
                          "../sortedSentence.txt",
                          sortedSentence,
                          "utf8",
                          (error) => {
                            if (error) {
                              callBack(error);
                            }
                            fs.appendFile(
                              //appending file name inside my fileNames.txt file
                              fileNamePath,
                              "sortedSentence.txt",
                              "utf8",
                              (error) => {
                                if (error) {
                                  callBack(error);
                                }
                                fs.readFile(
                                  //reading data inside txt file
                                  fileNamePath,
                                  "utf8",
                                  (error, data) => {
                                    if (error) {
                                      callBack(error);
                                    }
                                    let files = data.split("\n"); //splitting and storing all files names inside array
                                    files.forEach((value) => {
                                      //iterating inside array using forEach
                                      fs.unlink(`../${value}`, (error) => {
                                        //deleting files
                                        if (error) {
                                          callBack(error);
                                        }
                                      });
                                    });
                                    console.log("deleted successfully");
                                  }
                                );
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          });
        });
      });
    }
  });
}
module.exports = readFile; //exporting function
