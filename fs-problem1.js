const fs = require("fs"); //importing fs module
const path = require("path"); //importing path module
function createAndDeleteFiles(directoryPath, numberOfFiles, callBack) {
  //created function
  fs.mkdir(directoryPath, (error) => {
    //created directory in directoryPath using mkdir method
    if (error) {
      callBack(error); //if error occurs calling callBack function
    } else {
      console.log("directory created");
      for (let fileNumber = 1; fileNumber <= numberOfFiles; fileNumber++) {
        //running loop till numberOfFiles
        let fileName = `file${fileNumber}.json`; //created a file name
        let insertData = {
          //created a object and inserted data
          number: Math.random(),
        };
        let filePath = path.join(directoryPath, fileName); //using path module we created path for the file
        fs.writeFile(filePath, JSON.stringify(insertData), (error) => {
          //help of writeFile we are writing data to a file and converting data to json type
          if (error) {
            callBack(error);
          }
        });
      }

      fs.readdir(directoryPath, (error, files) => {
        //readdir is used to read data inside directory.
        if (error) {
          callBack(error);
        } else {
          files.forEach((value) => {
            // readdir will return all files inside directory in form of array so we are iterating inside array using forEach
            let filePath = path.join(directoryPath, value); //assigned file path
            fs.unlink(filePath, (error) => {
              //unlink means deleting files
              if (error) {
                callBack(error);
              }
            });
          });
          console.log("deleted files successfully");
        }
      });
    }
  });
}
module.exports = createAndDeleteFiles; //exporting function
