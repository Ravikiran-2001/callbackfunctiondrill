const createAndDeleteFiles = require("../fs-problem1.js"); //importing function
const path = "../test/random"; //path where directory will be created
const num = 6; //number of files to be created
createAndDeleteFiles(path, num, (error) => {
  //calling function and implementing callBack function
  if (error) {
    console.log(error);
  } else {
    console.log("succesfully created");
  }
});
